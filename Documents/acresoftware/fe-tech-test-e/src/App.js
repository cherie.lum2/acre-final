import { isEmpty } from "lodash";
import React, { Component, setState } from "react";
import { ApolloProvider } from "react-apollo";
import { gql, useQuery } from '@apollo/client';


import logo from "./acre-logo.svg";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state={
      users:[],
      role:"ALL",
    };
  }

  

  componentDidMount() {
    this.getUsers(this.state.role);
  }

  getUsers(role=undefined) {
    this.setState({users: []});

    const axios = require('axios');

    let data;

    if(role && role !== 'ALL') {
      data = JSON.stringify({
        query: `query {
          users (role: "${role}"){ 
              name
              role
              createdAt
              permsissions {createCustomer}
          }
      }`,
        variables: {}
      });
    } 
    else {
      data = JSON.stringify({
        query: `query {
          users {
              name
              role
              createdAt
              permsissions {createCustomer}
          }
      }`,
        variables: {}
      });
    }

    const config = {
      method: 'post',
      url: 'http://localhost:4000/',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };

    axios(config)
    .then((response) => {
      const usersArr = response.data.data.users;
      this.setState({users: usersArr});
    })
    .catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <div className="app" >
        <header className="app-header" >
          <img src={logo} className="app-logo" alt="logo" />

          <h1> Welcome to acre </h1>

          <h2>Users</h2>

          <select onChange= {(e)=>{
            this.setState({role: e.target.value}, () => {
              this.getUsers(this.state.role);
            });
          }}>
            <option value="ALL">All Users</option>
            <option value="ADMIN">Admin</option>
            <option value="BROKER">Broker</option>
            <option value="ADVISOR">Advisor</option>
          </select>
          <ul>
            {this.state.users.map((user)=>{
              // console.log(user)
              const roles = user.role;
              

              let roleNames = ""
              // console.log(roles);
              for (let i = 0; i< roles.length; i++) {
                roleNames += roles[i] + " ";
              }

              let name = user.name

              if (name === undefined || name === "") {
                return <></>
              }

              // if ()
                return <div class="customerInfo">
                        <h2>{name}</h2> 
                        <h4>{roleNames}</h4>
                        {user.permsissions.createCustomer === true && 
                        <button class="cardButton"> Create Customer</button> }
                      </div>
              // else
              //   return <div><li>{name} <strong>{roleNames}</strong></li></div>
            })}
            {/* <li>
              John <strong>Admin</strong>
            </li>
            <li>
              Mary <strong>Admin</strong>
            </li> */}
          </ul>
        </header>
      </div>
    );
  }
}

export default App;
